#include "Led_indicator.h"

#define MILLIS 1000
#define TIMER 0

CRGB leds[NUMBER_OF_LEDS];
hw_timer_t * timer = NULL;

bool blinking = false;
int blink_duration = 0;
CRGB current_color = CRGB::White;

volatile bool timer_triggered = false;
volatile bool led_is_off = false;

void IRAM_ATTR on_timer_end() {
  timer_triggered = true;
}

void timer_init(){
  // Configure the Prescaler at 80 the quarter of the ESP32 is cadence at 80Mhz
  // 80000000 / 80 = 1000000 tics / seconde
  timer = timerBegin(TIMER, 80, true);
  timerAttachInterrupt(timer, &on_timer_end, true);
}

void start_timer(int duration){
  timerAlarmWrite(timer, duration*MILLIS , false);
  timerAlarmEnable(timer);
}

void stop_timer(){
  timerAlarmDisable(timer);
  timerRestart(timer);
}

void set_led(CRGB color = CRGB::White,unsigned int duration_ms = 0,bool blink = false){
  stop_timer();
  blinking = blink;
  if (blinking) {
    blink_duration = duration_ms;
  }else{
    blink_duration = 0;
  }
  FastLED.showColor(color);
  if (duration_ms > 0) {
    start_timer(duration_ms);
  }
}

void led_blink(CRGB color,unsigned int duration_ms){
  led_is_off = false;
  current_color = color;
  set_led(color, duration_ms, true);
}

void led_on(CRGB color,unsigned int duration_ms){
  led_is_off = false;
  set_led(color, duration_ms, false);
}

void led_off(){
  led_is_off = true;
  set_led(CRGB::Black);
}

void led_poll_status(){
  if (timer_triggered) {
    timer_triggered = false;
    if (led_is_off) {
      set_led(current_color, blink_duration, blinking);
    }else{
      set_led(CRGB::Black, blink_duration, blinking);
    }
    led_is_off = !led_is_off;
  }
}

void init_led(){
  FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, NUMBER_OF_LEDS);
  timer_init();
}
