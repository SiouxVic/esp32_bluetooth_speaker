#pragma once

#include "FastLED.h"

#define LED_PIN 19
#define NUMBER_OF_LEDS 1

void led_on(CRGB color = CRGB::White,unsigned int duration_ms = 0);
void led_blink(CRGB color = CRGB::White,unsigned int duration_ms = 500);
void led_off();
void led_poll_status();
void init_led();
