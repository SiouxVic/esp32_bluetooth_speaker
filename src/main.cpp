#include <Arduino.h>
#include "BluetoothA2DPSink.h"
#include "Led_indicator.h"

// ------ Board pin definition ------
#define LED_PIN GPIO_NUM_19

#define POWER_BUTTON GPIO_NUM_32
#define DISCONNECT_BUTTON GPIO_NUM_10
#define BATTERY_STATUS_BUTTON GPIO_NUM_23

#define DIN_PIN GPIO_NUM_27
#define BCK_PIN GPIO_NUM_26
#define LCK_PIN GPIO_NUM_25

// ------ Constants -------
#define BLUETOOTH_NAME "DEV_TEST"
#define TIMEOUT_MINUTES 2
#define TIMEOUT_DELAY (1000 * 60 * TIMEOUT_MINUTES)

// ------- Globals -------
BluetoothA2DPSink a2dp_sink;
unsigned long shutdown_ms = millis() + TIMEOUT_DELAY;

// ------ Power management -----
void deep_sleep(){
  a2dp_sink.end();
  esp_sleep_enable_ext0_wakeup(POWER_BUTTON, LOW);
  Serial.println("Going to sleep. ZZZzzzz....");
  led_off();
  esp_deep_sleep_start();
}

void on_data() {
  shutdown_ms = millis() + TIMEOUT_DELAY;
}

void check_autoshutdown_timeout(){
  if (millis()>shutdown_ms){
    deep_sleep();
  }
}

// ------ Bluetooth management -----
void on_BT_disconnected(){
  led_blink(CRGB::Blue, 400);

}

void on_BT_connected(){
  Serial.println("we are connected!");
  led_on(CRGB::Blue, 2000);
}

void check_bluetooth_connection(){
  if (a2dp_sink.is_connected()) {

  }else{
    led_blink(CRGB::Blue, 400);
  }
}

void on_BT_status_changed(esp_a2d_connection_state_t state, void *ptr){
  Serial.print("State changed, new state: ");
  Serial.println(a2dp_sink.to_str(state));

  switch (state) {
    case ESP_A2D_CONNECTION_STATE_DISCONNECTED:
      on_BT_disconnected();
      break;
    case ESP_A2D_CONNECTION_STATE_CONNECTED:
      on_BT_connected();
      break;
    case ESP_A2D_CONNECTION_STATE_CONNECTING:

      break;
    case ESP_A2D_CONNECTION_STATE_DISCONNECTING:

      break;
  }
}

void init_BT(){
  Serial.println("Initializing bluetooth.");
  a2dp_sink.set_on_connection_state_changed(on_BT_status_changed);
  a2dp_sink.set_on_data_received(on_data);

  i2s_pin_config_t my_pin_config = {
      .bck_io_num   = BCK_PIN,
      .ws_io_num    = LCK_PIN,
      .data_out_num = DIN_PIN,
      .data_in_num  = I2S_PIN_NO_CHANGE
  };
  a2dp_sink.set_pin_config(my_pin_config);

  Serial.print("Bluetooth name: ");
  Serial.println(BLUETOOTH_NAME);

  //By default the ESP will try to connect to the last connected device.
  a2dp_sink.start(BLUETOOTH_NAME);

}

// ------ Inputs management ------

bool is_button_pressed(int pin){
  if (digitalRead(pin) == LOW) {
    delay(50); //Debounce
    if (digitalRead(pin) == LOW) { //Verification after debounce
      return true;
    }
  }
  return false;
}

void wait_for_release(int pin){
  while (!is_button_pressed(pin)) {
    delay(10);
  }
}

void on_power_button_pressed(){
  //Waiting for the button release, Otherwise the ESP32 restart immediatly after
  //going in deep sleep.
  wait_for_release(POWER_BUTTON);
  delay(200); //Debounce;
  deep_sleep();
}

void on_disconnect_button_pressed(){
  wait_for_release(DISCONNECT_BUTTON);
  a2dp_sink.disconnect();
}

void on_battery_status_button_pressed(){

}

void check_button_state(){
  if (is_button_pressed(POWER_BUTTON)) {
    on_power_button_pressed();
  }
  if (is_button_pressed(DISCONNECT_BUTTON)) {
    on_disconnect_button_pressed();
  }
  if (is_button_pressed(BATTERY_STATUS_BUTTON)) {
    on_battery_status_button_pressed();
  }
}

void init_buttons(){
  pinMode(POWER_BUTTON, INPUT_PULLUP);
  pinMode(DISCONNECT_BUTTON, INPUT_PULLUP);
  pinMode(BATTERY_STATUS_BUTTON, INPUT_PULLUP);
}

// ------ Main Program ------
void setup() {
  Serial.begin(115200);
  Serial.println("Starting!");

  init_buttons();
  init_BT();
  init_led();
  check_bluetooth_connection();
}

void loop() {
  check_button_state();
  led_poll_status();
  delay(10);
}
